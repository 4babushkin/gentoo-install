## Gentoo installation guide EFI, RAID, LVM

[[_TOC_]]

#### Download, and boot from live cd disk. [stage3](https://mirror.yandex.ru/gentoo-distfiles/releases/amd64/autobuilds/current-stage3-amd64/)

#### Network config

###### Determine interface names

Show names of netwok device :
```bash
ls /sys/class/net
eth0  lo
```
or
```bash
ifconfig -a
eth0      Link encap:Ethernet  HWaddr FE:FD:00:00:00:00  
          BROADCAST NOARP MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:0 (0.0 b)  TX bytes:0 (0.0 b)
```

###### Config network
- Automation network config
```bash
net-setup eth0
```

- Manual network config
```bash
ifconfig eth0 ${IP_ADDR} broadcast ${BROADCAST} netmask ${NETMASK} up
route add default gw ${GATEWAY}
```

Testing the network
```bash
ping -c 3 gentoo.org
PING gentoo.org (89.16.167.134) 56(84) bytes of data.
64 bytes from www.gentoo.org (89.16.167.134): icmp_seq=1 ttl=54 time=95.7 ms
64 bytes from www.gentoo.org (89.16.167.134): icmp_seq=2 ttl=54 time=96.0 ms
64 bytes from www.gentoo.org (89.16.167.134): icmp_seq=3 ttl=54 time=95.2 ms
```

If dosen`t work, config DNS server
```bash
tee -a /etc/resolv.conf <<EOF
nameserver 8.8.8.8
nameserver 1.1.1.1
EOF
```


#### Config ssh access 

Set root password and start sshd
```bash
passwd
/etc/init.d/sshd start
```

connect over ssh
```
ssh root@<ip-address>
```
---
#### Preparing the disks GPT

list block devices
```
lsblk
NAME  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0   7:0    0 379.3M  1 loop /mnt/livecd
sda     8:0    0    32G  0 disk 
sdb     8:16   0    32G  0 disk 
sr0    11:0    1   417M  0 rom  /mnt/cdrom
```

start `fdisk` 
```
fdisk -t gpt /dev/sda
>g (create a new empty GPT partition table)

>n
>1
> (enter)
>+512M
>t
>1
>1 (EFI System)

>n
>2
> (enter)
>+15G

>n
>3
> (enter)
>+3G
>t
>3
>19 (Linux swap)

>n
>4
> (enter)
> (enter)

>w (write table to disk and exit) 
```

p   print the partition table
```
Command (m for help): p
Disk /dev/sda: 40 GiB, 42949672960 bytes, 83886080 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 8E9EDE85-549A-BE49-A502-135AAA9ADDC4

Device        Start       End  Sectors  Size Type
/dev/sda1      2048   1050623  1048576  512M EFI System
/dev/sda2   1050624  32507903 31457280   15G Linux filesystem
/dev/sda3  32507904  38799359  6291456    3G Linux swap
/dev/sda4  38799360 104857566 66058207 31.5G Linux filesystem
```

Now copy GPT partition to /dev/sdb
```
sgdisk /dev/sda -R /dev/sdb
sgdisk -G /dev/sdb
```
---
#### Create RAID1

```
modprobe raid1
```

> Clear superblock if disk was use previously
> ```mdadm --zero-superblock --force /dev/sd{a,b}1```

> Пересобираем RAID если перезагрузились и не доустановили
>```
> # смотрим имена устройст raid
> lsblk
> ## останавливаем 
> mdadm --stop md124 
> mdadm --stop md125
> mdadm --stop md126
> mdadm --stop md127
> # Смотрим raid
> mdadm --detail --scan
> 
> mdadm --assemble /dev/md1 --name=boot_raid1 --update=name /dev/sda1 /dev/sdb1
> mdadm --assemble /dev/md2 --name=rootfs_raid1 --update=name /dev/sda2 /dev/sdb2
> mdadm --assemble /dev/md3 --name=swap_raid1 --update=name /dev/sda3 /dev/sdb3
>```

> Sub-versions of the version-1 superblock
> ```
> 0.9 	At the end of the device
> 1.0 	At the end of the device
> 1.1 	At the beginning of the device
> 1.2 	4K from the beginning of the device
> ```

```
mdadm --create --verbose /dev/md1 --level=1 --raid-devices=2 --metadata=1.0 --name="boot_raid1" /dev/sda1 /dev/sdb1
mdadm --create --verbose /dev/md2 --level=1 --raid-devices=2 --metadata=1.2 --name="rootfs_raid1" /dev/sda2 /dev/sdb2
mdadm --create --verbose /dev/md3 --level=1 --raid-devices=2 --metadata=1.2 --name="swap_raid1" /dev/sda3 /dev/sdb3
mdadm --create --verbose /dev/md4 --level=1 --raid-devices=2 --metadata=1.2 --name="lvm_raid1" /dev/sda4 /dev/sdb4
```

Watch the synchronization
```
watch -n 1 cat /proc/mdstat
```
Creating file systems
```
mkfs.fat -F 32 /dev/md1
mkfs.ext4 /dev/md2
mkswap /dev/md3
swapon /dev/md3
pvcreate /dev/md4
vgcreate vg00 /dev/md4
```

Mounting partitions
```
mount /dev/md2 /mnt/gentoo
```

---
#### Installing a stage3 

Verify the current date and time by running the `date` command:
```
date
Thu Jan 28 06:36:23 UTC 2021
```

If the date/time displayed is wrong, update it.
```
date MMDDhhmmYYYY
```
or Automatic
```
ntpd -q -g
```

#### Installing stage tarball

Downloading the stage tarball
```
cd /mnt/gentoo

wget https://mirror.yandex.ru/gentoo-distfiles/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-20210210T214503Z.tar.xz
```

Extracting archive 
```
tar xpf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
```

Safe RAID mdadm config
```
mdadm --examine --scan >> /mnt/gentoo/etc/mdadm.conf
```


#### Configuring compile options

set a couple of variables in `nano /mnt/gentoo/etc/portage/make.conf`

```
COMMON_FLAGS="-march=native -O2 -pipe"
```

> number of processors `grep processor /proc/cpuinfo`. Number of CPU plus one.
```
MAKEOPTS="-j7"
```
> show `CPU_FLAGS_X86` like this `lscpu` or `cat /proc/cpuinfo`
```
CPU_FLAGS_X86="aes avx mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
```

GRUB EFI 64 bit platform support
```
GRUB_PLATFORMS="efi-64"
```

```
COMMON_FLAGS="-march=native -O2 -pipe"
MAKEOPTS="-j7"
GRUB_PLATFORMS="efi-64"

CPU_FLAGS_X86="aes avx mmx mmxext pclmul popcnt rdrand sse sse2 sse3 sse4_1 sse4_2 ssse3"
```

---
#### Installing the Gentoo base system

Select mirror
```
mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
```
Repo config
```
mkdir -p /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
```

Copy DNS info
`cp --dereference /etc/resolv.conf /mnt/gentoo/etc/`

Mounting the necessary filesystems

```
mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev 
```

Entering the new environment

```
chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chroot) ${PS1}"
```

Mounting the boot partition
```
mount /dev/md1 /boot
```

Синхронизация пакетов /usr/db/portage 
```
emerge-webrsync
emerge --sync
```

Выбор подходящего профиля
```
eselect profile list
```
[1]   default/linux/amd64/17.1 (stable) *
```
eselect profile set 1   
```

> Просмотр USE-флагов, которые используются для выбранного профиля `emerge --info | grep ^USE`
> В файле /etc/portage/make.conf можно добавлять, убирать USE-флаги. Если это произошло, то необходимо пересобрать мир следующей командой `emerge -auND @world`


> Проверка
> 
> ```
> emerge -quDNpv @world
> ```
> Из за gcc новой версии долго собирается, оставлю старую
> 
> Маскирую (Скрою) установку пакета
> `nano /etc/portage/package.mask/sys-deval`
> 
> и вставим
> ```
> >=sys-devel/gcc-10.2.0-r5
> ```

Обновление пакетов
```
emerge --ask --verbose --update --deep --newuse @world
```
или такая команда
```
emerge -auvDN @world
```

##### Часовой пояс

Просмотреть доступные варианты:
```
ls /usr/share/zoneinfo
```
Например, для Москвы настройка будет такой:
```
echo "Europe/Moscow" > /etc/timezone
```
На основании записи в /etc/timezone сгенерируем /etc/localtime.
```
emerge --config sys-libs/timezone-data
```

##### Настройка локали
```
nano -w /etc/locale.gen
```
/etc/locale.gen
```
en_US ISO-8859-1
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
```

Генерируем
```
locale-gen
```

Теперь просмотрим список доступных локалей:
```
eselect locale list
Available targets for the LANG variable:
  [1]   C
  [2]   C.utf8
  [3]   en_US
  [4]   en_US.iso88591
  [5]   en_US.utf8 *
  [6]   POSIX
  [7]   ru_RU.utf8
  [ ]   (free form)
```

выбираем свою en_US.utf8
```
eselect locale set 5
```

Заново перезагрузите окружение: 
```
env-update && source /etc/profile && export PS1="(chroot) ${PS1}"
```

#### Собираем ядро

##### Установка исходников

```
emerge --ask sys-kernel/gentoo-sources
```
Данная команда установит исходный код ядра Linux в /usr/src/, в котором символьная ссылка linux будет указывать на установленную версию: 
```
ls -l /usr/src/linux
```

##### genkernel
```
emerge --ask sys-kernel/genkernel
```
Если возникла проблемма с лицензией
```
etc-update
>1
>q
>1
```

###### ставим genkernel mdadm lvm2 tmux mc
```
emerge --ask sys-kernel/genkernel sys-fs/lvm2 sys-fs/mdadm app-misc/tmux app-misc/mc 
```
добавляем в загрузку
```
rc-update add lvm boot
```

> не знаю надо ли делать действие ниже или нет. LVM вообще в ядро не надо добавлять, потому что мы не будем грузиться с LVM

Надо настроить `/etc/genkernel.conf` 
```
LVM="yes”
MDADM="yes”
MDADM_CONFIG="/etc/mdadm.conf" 
```


Теперь надо прописать /boot в /etc/fstab. [EFI_System_Partition](https://wiki.gentoo.org/wiki/EFI_System_Partition)

Можно подсмотреть параметры для монтирования  `mount | egrep "md"`

```
nano -w /etc/fstab
```

/etc/fstab
```
/dev/md1               /boot           vfat            noauto,noatime  0 2
```

Запускаем компиляцию ядра:

```
genkernel --menuconfig --lvm --mdadm all
``` 

Then make sure to select:
```
General setup > [*] Initial RAM filesystem and RAM disk (initramfs/initrd) support
Enable the block layer > Partition Types > [*]   EFI GUID Partition support
Processor type and features > [*] EFI runtime service support
Device Drivers > Generic Driver Options > () path to uevent helper
Device Drivers > Multiple devices driver support (RAID and LVM) > <*>   RAID support 
Device Drivers > Multiple devices driver support (RAID and LVM) > <*>   RAID-1 (mirroring) mode
Device Drivers > Multiple devices driver support (RAID and LVM) > <*>   Device mapper support > <*>     Crypt target support
Device Drivers > Multiple devices driver support (RAID and LVM) > <*>   Device mapper support > <*>     Mirror target
Device Drivers > Graphics support > Support for frame buffer devices > [*]   Enable firmware EDID
Device Drivers > Graphics support > Support for frame buffer devices > [*]   EFI-based Framebuffer Support
Device Drivers > Graphics support > Console display driver support > <*> Framebuffer Console support
Firmware Drivers > EFI (Extensible Firmware Interface)  Support > <*> EFI Variable Support via sysfs
```

Настройки касающиеся виртуализации
```
[*] Virtualization  --->
    <M>   Kernel-based Virtual Machine (KVM) support
    <M>     KVM for Intel processors support
    <M>     KVM for AMD processors support
    [*]     Audit KVM MMU
    <M>   Host kernel accelerator for virtio net (EXPERIMENTAL)
Device Drivers  --->
    [*] Network device support  --->
        <*>     MAC-VLAN support
        <*>       MAC-VLAN based tap driver
        <M>     Universal TUN/TAP device driver support
[*] Networking support  --->
    Networking options  --->
        <M> 802.1d Ethernet Bridging
        [*] Network packet filtering framework (Netfilter)  --->
            [*]   Advanced netfilter configuration
            Core Netfilter Configuration  --->
                <M>   "physdev" match support
                
Security options  --->
    Grsecurity  --->
        Virtualization Type (Host)
        Virtualization Hardware (EPT/RVI Processor Support)
        Virtualization Software (KVM)
        Required Priorities (Performance)
```

Запишите название файлов ядра и initrd

```
ls /boot/kernel* /boot/initramfs*
```

#### Настройка системы

###### Полный пример /etc/fstab
```
/dev/md1               /boot           vfat            defaults  0 2
/dev/md2               /               ext4            defaults,noatime  0 1
/dev/md3               none            swap            sw                0 0
```

#### Информация о сети

```
nano -w /etc/conf.d/hostname

hostname="gentoo2"

```

#### Настройка сети

 Сначала установите
```
emerge --ask --noreplace net-misc/netifrc
```
Если надо настоить вручную
```
nano -w /etc/conf.d/net
```

```
dns_domain_lo="localhost"

##DHCP example
##config_enp0s3="dhcp"

## static
config_enp0s3="192.168.10.65/24"
routes_enp0s3="default via 192.168.10.1"
dns_servers_enp0s3="192.168.10.1"
```

Автоматический запуск сетевого подключения при загрузке системы
```
cd /etc/init.d 
ln -s net.lo net.enp0s3
rc-update add net.enp0s3 default
```

Файл hosts `nano /etc/hosts`

```
127.0.1.1  gentoo2 
```

Пароль суперпользователя в новой системе

```
passwd
```

Настроим вход для root `nano -w /etc/ssh/sshd_config`
```
PermitRootLogin yes
```


#### Инициализация и конфигурация загрузки

настройки сервисов запуска OpenRC `nano -w /etc/rc.conf`
```
rc_parallel="YES"
```
настройки раскладки клавиатуры `nano -w /etc/conf.d/keymaps` переключение будет происходить по CapsLock

keymap="-u ruwin_cplk-UTF-8"

Настройка часов: `nano -w /etc/conf.d/clock`

```
CLOCK="local"
TIMEZONE="Europe/Moscow"
```
#### Установка системных средств

```
emerge -qv sysklogd cronie mlocate e2fsprogs dosfstools dhcpcd
```
Добавим их в уровень запуска по умолчанию с помощью rc-update
```
rc-update add sysklogd default
rc-update add cronie default
crontab /etc/crontab
rc-update add sshd default
```

#### Install Bootloader
1. Install grub
`## emerge -av sys-boot/grub:2`

2. Install EFI
```
grub-install --target=x86_64-efi --efi-directory=/boot

grub-install --target=x86_64-efi --efi-directory=/boot --removable
```

3. Create configuration
`## grub-mkconfig -o /boot/grub/grub.cfg`

Reboot!
```
## exit
## cd
## umount -l /mnt/gentoo/dev{/shm,/pts,}
## umount -R /mnt/gentoo 
## reboot
```

#### [Add user.](adduser)

#### [Update gentoo] (https://wiki.gentoo.org/wiki/Upgrading_Gentoo/ru)
