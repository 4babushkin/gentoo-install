#!/bin/bash

# const
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
# vars
DEV="/dev/sda2"

######

if grep -qs '/mnt/gentoo ' /proc/mounts; then
    echo -e "${Cyan} /mnt/gentoo mounted."
else
    echo -e "${Cyan} Mount /mnt/gentoo"
    mount $DEV /mnt/gentoo
fi


mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
chroot /mnt/gentoo /bin/bash
source /etc/profile
export PS1="(chROOT) ${PS1}"
